const Sequelize = require('sequelize');

const sequelize = new Sequelize(null, null, null, {
    dialect: 'sqlite',
    storage: './database.sqlite',
    define: {
        timestamps: true,
        freezeTableName: true,
    }
});

/*******
 * TODO: Definição dos modelos.
 * Defina aqui os modelos a serem mapeados para entidades do banco de dados.
 *******/
const Usuario = sequelize.define('usuario', {
            id: {
            primaryKey: true,
            type: Sequelize.BIGINT,
            autoIncrement: true,
        },
        nome: {
            type: Sequelize.STRING(200),
            allowNull: false,
        },
       
        email: {
            type: Sequelize.STRING(200),
            unique: true,
        },
        cpf: {
            type: Sequelize.STRING(11),
            allowNull: false,
        },
        senha: {
            type: Sequelize.STRING(200),
            allowNull: false,
        },
        nascimento: Sequelize.DATEONLY
    });
    


const Tarefa = sequelize.define('tarefa', {
     id: {
            primaryKey: true,
            type: Sequelize.BIGINT,
            autoIncrement: true,
        },
        titulo: {
            type: Sequelize.STRING(200),
            allowNull: false,
        },
       
        descricao: {
            type: Sequelize.TEXT,
            allowNull: false,
        },
        concluida: {
            type: Sequelize.TINYINT,
            allowNull: false,
        },
        usuarioId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        }
})

/*******
 * TODO: Definição das relações.
 * Defina aqui os relacionamentos entre os modelos.
 *******/

/*Usuario.hasMany(Tarefa, {
    // ...
})*/

Usuario.hasMany(Tarefa, {foreignKey: 'usuarioId'})
Tarefa.belongsTo(Usuario, {foreignKey: 'usuarioId'})
module.exports = {
    sequelize,
    Usuario,
    Tarefa,
};
