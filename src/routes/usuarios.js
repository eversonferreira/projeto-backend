const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const SECRET_KEY = '4wd!RV";9~^Ud]lDNWAYz19tw<6?GVl"/4Dvq0-=]CFnjTuB]e';

const validateSchema = require('./validateSchema');
const controller = require('../controllers/usuarios');

const { Usuario } = require('../models');


/*******
 * TODO: Definição das rotas do CRUD de Usuários e Login.
 * Exemplo:
 * 
 * const validateBody = {
 *   // Schema de validação do Express Validator
 * };
 * 
 * 
 * router.post('/',
 *   validateSchema(validateBody),
 *   controller.cadastro
 * );
 *******/
//Metodo de login

router.post('/login', function (request, response) {
    const { body } = request;
    const { email, senha } = body;

    //sess=req.session;
    // search within a specific range

    Usuario.count({ 
                where: { email: email, senha: { $eq: senha } } }).then(usuario => {
        if (usuario === 1) {

    /*Usuario.findOne({
           where:{
               email:email
           }
       }).then(ID =>{
        console.log(ID);
       });*/
       Usuario.findOne({
        where:{
            email:email
        }
    }).then(datas => {
       // console.log(dastas.id);
       let id = (datas.id);
      // console.log (id_user);
    
           //Usuario logado
       const payload = {
        email: email,
        id
    }

               const token = jwt.sign(payload, SECRET_KEY)
            
            response.status(200).json({
                token: token
                 });
                // console.log (id_user);
                });
        } 
        else {
            response.status(401).send('Usuário ou senha incorretos.');
        }
    });

  
    
});

//Metodo retorna usuarios se o usuario estiver validado email e senha, posteriormente
//deve-se inserir o token recebido

router.get('/', function(req, res, next) {
    // res.send('respond with a resource');
    const { token } = req.headers;

   
    Usuario.findAll({attributes: ['id','nome', 'email','cpf','nascimento']}).then(usuarios=>{

    try {
        const payload = jwt.verify(token, SECRET_KEY)
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        //res.status(200).send('Acesso Permitido.');
      
        res.status(200).json(usuarios);
        
      } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')  }
    });
    
   });

//Metodo utilizado para inserir novos usuarios

router.post('/', function (request, response) {
    const { body } = request;

    const { nome, email, cpf, senha, nascimento } = body;

    Usuario.create({
        nome, email, cpf, senha, nascimento
    })
        .then(usuario => {
            response.status(201).json(usuario);
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi possível incluir o registro.')
        })
});

//******Buscar Usuario por ID **********//
router.get('/:usuarioId', function (request, response) {
    const { params } = request;
    const { usuarioId } = params

    Usuario.findById(usuarioId)
        .then(usuario => {
            if (!usuario) {
                response.status(404).send('Usuario não encontrado.')
            } else {
                response.status(200).json(usuario);
            }
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi possível consultar o usuario');
        })
})


//******Atualização por ID **********//
router.put('/:usuarioId', function (request, response) {
    const { params, body } = request
    const { usuarioId } = params

    const { nome, email, cpf, senha, nascimento } = body;

    Usuario.findById(usuarioId)
        .then(usuario => {
            if (!usuario) {
                response.status(404).send('Usuario não encontrado.')
            } else {
                return usuario.update({
                    nome, email, cpf, senha, nascimento
                })
                    .then(() => {
                        response.status(200).json(usuario);
                    })
            }
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi Atualizar consultar o usuario');
        })

})





module.exports = router;
