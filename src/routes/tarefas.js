const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const SECRET_KEY = '4wd!RV";9~^Ud]lDNWAYz19tw<6?GVl"/4Dvq0-=]CFnjTuB]e';
//const { authenticationMiddleware } = require('../utils/token');
const validateSchema = require('./validateSchema');
const controller = require('../controllers/tarefas');

const { Tarefa } = require('../models');
const { Usuario } = require('../models');

/*******
 * TODO: Definição das rotas do CRUD de Tarefas.
 * Exemplo:
 * 
 * const validateBody = {
 *   // Schema de validação do Express Validator
 * };
 * 
 * 
 * router.post('/',
 *   validateSchema(validateBody),
 *   authenticationMiddleware,
 *   controller.cadastro,
 * );
 *******/

//Metodo utilizado para inserir novos usuarios

/*router.post('/', function (request, response) {
    //const { token } = req.headers;
    const { body } = request;

    const { titulo, descricao } = body;

  Tarefas.create({
        titulo, descricao
    })
        .then(datas => {
            response.status(201).json(datas);
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi possível incluir o registro.')
        })
        console.log(titulo);
});*/
router.get('/', function (req, res, next) {
    // res.send('respond with a resource');
    const { token } = req.headers;

    const { body } = req;
    //const { titulo } = body;
    //let  titulo  = req.url;
    let titulo = req.query.titulo;

    try {
        const payload = jwt.verify(token, SECRET_KEY)
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        console.log('title: ', titulo);

        let str1 = "%";
        let pesquisa = str1.concat(titulo, str1)

        //res.status(200).send('Acesso Permitido.');
        Tarefa.findAll({
            where: { titulo: { $like: pesquisa } },

            include: [{
                model: Usuario,
                where: { id: payload.id }
            }]


        }).then(tarefas => {
            res.status(200).json(tarefas);

        });



    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }


});



router.get('/', function (req, res, next) {
    // res.send('respond with a resource');
    const { token } = req.headers;

    const { body } = req;
    //const { titulo } = body;
    //let  titulo  = req.url;
    let titulo = req.query.titulo;

    try {
        const payload = jwt.verify(token, SECRET_KEY)
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        console.log('title: ', titulo);

        let str = "%";
        let pesquisa = str.concat(titulo, str)

        //res.status(200).send('Acesso Permitido.');
        Tarefa.findAll({
            where: { titulo: { $like: pesquisa } },

            include: [{
                model: Usuario,
                where: { id: payload.id }
            }]


        }).then(tarefas => {
            res.status(200).json(tarefas);

        });



    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }


});


//******Buscar Tarefa por ID **********//
router.get('/:tarefaId', function (request, response) {
    const { params } = request;
    const { tarefaId } = params

    Tarefa.findById(tarefaId)
        .then(tarefa => {
            if (!tarefa) {
                response.status(404).send('Tarefa não encontrado.')
            } else {
                response.status(200).json(tarefa);
            }
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi possível consultar a tarefa');
        })
})






router.post('/', function (req, res, next) {
    const { body } = req;
    //const { token } = req.headers;

    const token = req.headers['x-access-token'];

    const { titulo, descricao } = body;
    const concluida = 0;


    try {
        const payload = jwt.verify(token, SECRET_KEY)

        const usuarioId = payload.id;
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        Tarefa.create({
            titulo, descricao, concluida, usuarioId
        })
            .then(tarefas => {
                res.status(201).json(tarefas);
            })
            .catch(ex => {
                console.error(ex);
                response.status(412).send('Não foi possível incluir o registro.')
            })

        // res.status(200).json(usuarios);

    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }
    
});


//******Atualização por ID **********//
router.put('/:tarefaId', function (request, response) {
    const { params, body } = request
    const { tarefaId } = params

    const { titulo, descricao } = body;

    Tarefa.findById(tarefaId)
        .then(tarefa => {
            if (!tarefa) {
                response.status(404).send('Tarefa não encontrado.')
            } else {
                return tarefa.update({
                    titulo, descricao
                })
                    .then(() => {
                        response.status(200).json(tarefa);
                    })
            }
        })
        .catch(ex => {
            console.error(ex);
            response.status(412).send('Não foi Atualizar consultar o usuario');
        })

})

// Excluir Tarefa

router.delete('/:tarefaId', function (req, res) {
    //const { body } = req;
    const token = req.headers['x-access-token'];
    const { params, body } = req
    const { tarefaId } = params

    try {
        const payload = jwt.verify(token, SECRET_KEY)

        //const usuarioId = payload.id;
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        Tarefa.destroy({
            where: { id: tarefaId, usuarioId: payload.id },

        })
            .then(tarefas => {
                if ((tarefas == 0)) {
                    res.status(412).send('Esse registro não pertence ao usuario.');
                } else {
                    res.status(201).send('Registro eliminado.');
                }

            })
            .catch(ex => {
                console.error(ex);
                res.status(412).send('Não foi possível excluir o registro.')
            })

        // res.status(200).json(usuarios);

    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }


});

router.put('/:tarefaId/:status', function (req, res) {
    const token = req.headers['x-access-token'];
    const { params, body } = req
    const { tarefaId } = params
    const { status } = params

    //const { titulo, descricao } = body;

    try {
        const payload = jwt.verify(token, SECRET_KEY)

        //const usuarioId = payload.id;
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        /*Tarefa.destroy({
            where: { id: tarefaId, usuarioId: payload.id },

        })*/
        //return 
        // verifica a tarefa o id do usuario
        Tarefa.count({
            where: { id: tarefaId, usuarioId: payload.id }
        }).then(usuario => {
            //se tarefa existe e pertence ao usuario 
            if (usuario === 1) {
                Tarefa.findById(tarefaId)

                    .then(tarefa => {
                        if (!tarefa) {
                            res.status(404).send('Tarefa não encontrado.')
                        } else {
                            return tarefa.update({

                                concluida: 1
                            })
                                .then(() => {
                                    res.status(200).json(tarefa);
                                })
                        }
                    })
                    .catch(ex => {
                        console.error(ex);
                        res.status(412).send('Não foi possível Atualizar consultar a tarefa');
                    })
            }
            // tarefa não encontrada ou de usuario diferente
            else {
                console.log('falso', usuario)
                res.status(401).send('Tarefa inexistente ou não pertence ao usuario')
            }

        });


    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }

})


router.delete('/:tarefaId/:status', function (req, res) {
    //const { body } = req;
    const token = req.headers['x-access-token'];
    const { params, body } = req
    const { tarefaId } = params
    const { status } = params

    try {
        const payload = jwt.verify(token, SECRET_KEY)

        //const usuarioId = payload.id;
        console.log('Token válido', payload);
        console.log('Acesso Permitido');
        Tarefa.destroy({
            where: { id: tarefaId, usuarioId: payload.id },

        })
            .then(tarefas => {
                if ((tarefas == 0)) {
                    res.status(412).send('Esse registro não existe ou não pertence a este usuario.');
                } else {
                    res.status(201).send('Registro eliminado.');
                }

            })
            .catch(ex => {
                console.error(ex);
                res.status(412).send('Não foi possível excluir o registro.')
            })

        // res.status(200).json(usuarios);

    } catch (exception) {
        console.error('Token inválido', exception);
        res.status(403).send('Acesso negado.')
    }


});

module.exports = router;
